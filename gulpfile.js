'use strict'

var gulp = require('gulp'),
concat = require('gulp-concat'),
sass = require('gulp-sass'),
uglifi = require('gulp-uglify');

gulp.task('concatJS', function(){
  gulp.src('static/a/src/js/lib/*.js')
    .pipe(concat("base.js"))
    .pipe(gulp.dest('static/a/js/'))

  gulp.src(['static/a/src/js/q/**/*.js'])
    .pipe(concat("app.js"))
    .pipe(gulp.dest('static/a/js'))

})

gulp.task('minJS', function(){
  gulp.src('static/a/js/base.js')
    .pipe(uglifi())
    .pipe(gulp.dest('static/a/js/min/'))

  gulp.src('static/a/js/app.js')
    .pipe(uglifi())
    .pipe(gulp.dest('static/a/js/min/'))
})

gulp.task('sass', function(){
  gulp.src(['static/a/src/sass/base.scss'])
    .pipe(sass())
    .pipe(gulp.dest('static/a/css/'))
})

gulp.task('watch', function(){
  gulp.watch("static/a/src/js/**/*.js", ['concatJS'])
  gulp.watch("static/a/src/sass/**/*.scss", ['sass'])
})

gulp.task('default', ['concatJS', 'minJS', 'sass'], function(){
});
