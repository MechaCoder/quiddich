# pip installed
from flask import Flask
from flask import render_template
from flask import jsonify
from flask import request

# PSL
from json import loads

# this codebase
import dataObjects

styleArray = ["/static/a/css/base.css"]
scrtptArray = ['/static/a/js/base.js']

app = Flask(__name__)


@app.route('/')
def index():
    return render_template(
        "index.html",
        pageTitle="Q - index",
        styles=styleArray,
        scripts=['/static/a/js/base.js']
    )

@app.route('/game/')
@app.route('/game/<league>')
def game(league="hogwarts"):
    # scrtptArray.append('/static/a/js/app.js')
    print('>> {}'.format(league))
    return render_template(
        "teamselect.html",
        pageTitle="Q - index",
        styles=styleArray,
        scripts=['/static/a/js/base.js', '/static/a/js/app.js'],
        teams=dataObjects.TeamsByLeague(league)
    )


@app.route('/ajax/getTeam/')
def ajaxGetTeam():
    try:
        teamName = request.args.get('name', '', type=str)
        data = dataObjects.getTeam(teamName)
        return jsonify({
            'sucsess': True,
            'message': "here is the team information",
            'data': data
        })
        pass
    except Exception as e:
        return jsonify({
            'sucsess': False,
            'message': "there has been an error",
            'data': e
        })


@app.route('/ajax/runGame/')
def ajaxRunGame():
    try:
        homeTeamName = request.args.get('home', '', type=str)
        awayTeamName = request.args.get('away', '', type=str)

        homeTeamObj = dataObjects.getTeam(homeTeamName)
        awayTeamObj = dataObjects.getTeam(awayTeamName)

        data = dataObjects.runGame(homeTeamObj, awayTeamObj)
        return jsonify({
            'sucsess': True,
            'message': 'game done',
            'data': data
        })
    except Exception as e:
        return jsonify({
            'sucsess': False,
            'message': "there has been an error",
            'data': e
        })
    pass


app.run(debug=True, port=8000, host='127.0.0.1')
