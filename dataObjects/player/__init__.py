#!/usr/bin/env python
from random import randint


def genSkill():
    return randint(0, 100)


def maxDict(_dict={}):
    return max(_dict, key=_dict.get)


def parsePosistion(_maxSkill=""):
    rString = ""
    if _maxSkill == "STR":
        rString = "beater"

    if _maxSkill == "PER":
        rString = "seeker"

    if _maxSkill == "END":
        rString = "keeper"

    if _maxSkill == "AGI":
        rString = "chaser"
    return rString


def getRandomName():
    firstName = [
        "Millicent", "Susannah", "Mirabelle", "Theodora",
        "Rosalind", "Zadie", "Aviva", "Devon",
        "Indigo", "Cleo", "Pilar", "Pippa", "Georgiana",
        "Octavia", "Zinnia", "Winifred", "Marguerite",
        "Henrietta", "Greer", "Snow", "Lulu",
        "Tierney", "Verity", "Juno", "Romy",
        "Dinah", "Seren", "Cassia", "Maple",
        "Polly", "Olympia", "Augusta", "Ilaria",
        "Vesper", "Vita", "Nico", "Saskia",
        "Leonora", "Dorothea", "Fay", "Dorian",
        "Mamie", "Eulalia", "Marietta", "Calypso",
        "Flannery", "Indra", "Tamsin", "Bronwen",
        "Fleur", "Lev", "Constantine", "Rio",
        "Shepherd", "Caspian", "Ellison", "Auden",
        "West", "Chester", "Aurelio", "Baxter",
        "Willis", "Baker", "Leopold", "Teo",
        "Stellan", "Roscoe", "Bram", "Cassidy",
        "Penn", "Octavius", "Slater", "True",
        "Nile", "Pax", "Everest", "Rufus",
        "Orson", "Casimir", "Laszlo", "Hawk",
        "Pascal", "Fitzgerald", "Shaw", "Mercer",
        "Ozias", "Bowie", "Larson", "Cosmo",
        "Ellington", "Hart", "Laird", "Griffith",
        "Whit", "Maguire", "Rafferty", "Sacha", "Florian",
        "Peregrine", "Poet", "Scorpus"
    ]

    secoundName = [
        "Annon", "Potter", "Weasly", "Malfoy", "Lovegood", "Belby",
        "Bones", " Buchanan", "Cattermole", "Chang", "Cooper", "Cram",
        "Crabble", "Crouch", "Delacour", "Diggory", "Dumbledore", " Edgecombe",
        "Flamel", "Flint", "Flume"
    ]

    return "{} {}".format(
        firstName[randint(0, len(firstName) - 1)],
        secoundName[randint(0, len(secoundName) - 1)]
    )


class Player(object):

    # self Decleration
    name = ""
    strength = 0  # if this is the most the player is a beater
    perception = 0  # seeker
    enduance = 0  # keeper
    agility = 0  # chaser
    posistion = ""
    captin = False

    def __init__(self, _name='', _str=0, _per=0, _end=0, _agi=0):
        """ a player """

        self.name = _name
        self.strength = _str
        self.perception = _per
        self.enduance = _end
        self.agility = _agi

        # if len(_name) == 0 or
        # _str == 0 or _per == 0 or _end == 0 or _agi == 0:
        self.generatePlayer()

    def __str__(self):
        return """
            Name: {},
            STR : {},
            PER : {},
            END : {},
            AGI : {},
            POSISTION : {}
            TEAM CAP'T: {}
        """.format(
            self.name,
            self.strength,
            self.perception,
            self.enduance,
            self.agility,
            self.posistion,
            self.captin
        )

    def getPlayerObject(self):
        temp = {
            "name": self.name,
            "posistion": self.posistion,
            "skills": {
                "STR": self.strength,
                "PER": self.perception,
                "END": self.enduance,
                "AGI": self.agility
            },
            "CAPT'N": self.captin
        }
        return temp

    def generatePlayer(self):
        """ generates a player """
        if len(self.name) == 0:
            self.name = getRandomName()
        if self.strength == 0:
            self.strength = genSkill()
        if self.perception == 0:
            self.perception = genSkill()
        if self.enduance == 0:
            self.enduance = genSkill()
        if self.agility == 0:
            self.agility = genSkill()
        if self.posistion == "":
            self.posistion = parsePosistion(maxDict({
                "STR": self.strength,
                "PER": self.perception,
                "END": self.enduance,
                "AGI": self.agility
            }))
        return True

    def importPlayerData(self, _name="", _str=0, _per=0, _end=0, _agi=0):
        if _name != "" and _str != 0 and _per != 0 and _end != 0 and _agi != 0:
            self.name = _name
            self.strength = _str
            self.perception = _per
            self.enduance = _end
            self.agility = _agi
            self.generatePlayer()
        else:
            pass
