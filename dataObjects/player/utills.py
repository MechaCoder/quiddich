from random import randint


def genSkill():
    return randint(0, 100)


def maxDict(_dict={}):
    return max(_dict, key=_dict.get)


def parsePosistion(_maxSkill=""):
    rString = ""
    if _maxSkill == "STR":
        rString = "beater"

    if _maxSkill == "PER":
        rString = "seeker"

    if _maxSkill == "END":
        rString = "keeper"

    if _maxSkill == "AGI":
        rString = "chaser"
    return rString


def getRandomName():
    firstName = [
        "Millicent", "Susannah", "Mirabelle", "Theodora",
        "Rosalind", "Zadie", "Aviva", "Devon",
        "Indigo", "Cleo", "Pilar", "Pippa", "Georgiana",
        "Octavia", "Zinnia", "Winifred", "Marguerite",
        "Henrietta", "Greer", "Snow", "Lulu",
        "Tierney", "Verity", "Juno", "Romy",
        "Dinah", "Seren", "Cassia", "Maple",
        "Polly", "Olympia", "Augusta", "Ilaria",
        "Vesper", "Vita", "Nico", "Saskia",
        "Leonora", "Dorothea", "Fay", "Dorian",
        "Mamie", "Eulalia", "Marietta", "Calypso",
        "Flannery", "Indra", "Tamsin", "Bronwen",
        "Fleur", "Lev", "Constantine", "Rio",
        "Shepherd", "Caspian", "Ellison", "Auden",
        "West", "Chester", "Aurelio", "Baxter",
        "Willis", "Baker", "Leopold", "Teo",
        "Stellan", "Roscoe", "Bram", "Cassidy",
        "Penn", "Octavius", "Slater", "True",
        "Nile", "Pax", "Everest", "Rufus",
        "Orson", "Casimir", "Laszlo", "Hawk",
        "Pascal", "Fitzgerald", "Shaw", "Mercer",
        "Ozias", "Bowie", "Larson", "Cosmo",
        "Ellington", "Hart", "Laird", "Griffith",
        "Whit", "Maguire", "Rafferty", "Sacha", "Florian",
        "Peregrine", "Poet", "Scorpus"
    ]

    secoundName = [
        "Annon", "Potter", "Weasly", "Malfoy", "Lovegood", "Belby",
        "Bones", " Buchanan", "Cattermole", "Chang", "Cooper", "Cram",
        "Crabble", "Crouch", "Delacour", "Diggory", "Dumbledore", " Edgecombe",
        "Flamel", "Flint", "Flume"
    ]

    return "{} {}".format(
        firstName[randint(0, len(firstName) - 1)],
        secoundName[randint(0, len(secoundName) - 1)]
    )
