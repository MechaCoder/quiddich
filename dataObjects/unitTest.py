from player import Player
from team import Team

import unittest


# testPlayer = Player()
# testPlayerOne = testPlayer.generatePlayer()


class Test_playerModual(unittest.TestCase):

    def test_object_WithoutModfication_type(self):
        """ test Object on inti without arguments type"""
        print("init without arguments")

        testPlayer = Player()

        self.assertTrue(type(testPlayer.name), type(""))
        self.assertTrue(type(testPlayer.strength), type(0))
        self.assertTrue(type(testPlayer.perception), type(0))
        self.assertTrue(type(testPlayer.enduance), type(0))
        self.assertTrue(type(testPlayer.agility), type(0))
        self.assertTrue(type(testPlayer.posistion), type(""))
        self.assertTrue(type(testPlayer.captin), type(True))

    def test_object_WithModfication_type(self):
        """ test object on init with argument"""
        print("init with aruments")

        testPlayer = Player('test player', 10, 20, 30, 40)
        self.assertTrue(type(testPlayer.name), type(""))
        self.assertTrue(testPlayer.name, "test player")
        self.assertTrue(testPlayer.strength, 10)
        self.assertTrue(testPlayer.perception, 20)
        self.assertTrue(testPlayer.enduance, 30)
        self.assertTrue(testPlayer.agility, 40)
        self.assertTrue(testPlayer.posistion, 'chaser')
        # self.assertTrue

    def test_method_getPlayerObject(self):
        """ test object export """
        print("test method getPlayerObject")

        testPlayer = Player('test player', 10, 20, 30, 40)
        testObject = testPlayer.getPlayerObject()

        self.assertTrue(type(testObject), type({}))
        self.assertTrue(type(testObject['name']), type(''))
        self.assertTrue(testObject['name'], 'test player')

        self.assertTrue(type(testObject['posistion']), type('chaser'))
        self.assertTrue(testObject['posistion'], 'chaser')

        self.assertTrue(type(testObject['skills']), type({}))
        self.assertTrue(type(testObject['skills']['STR']), type(0))
        self.assertTrue(type(testObject['skills']['PER']), type(0))
        self.assertTrue(type(testObject['skills']['END']), type(0))
        self.assertTrue(type(testObject['skills']['AGI']), type(0))
        self.assertTrue(type(testObject['skills']['AGI']), type(0))

    def test_method_generatePlayer(self):
        """ test method generatePlayer"""
        print("test method generatePlayer")

        testPlayer = Player()
        testObject = testPlayer.getPlayerObject()

        self.assertTrue(testObject, True)
        self.assertTrue(type(testPlayer.name), type(''))
        self.assertTrue(len(testPlayer.name) != 0, True)

        self.assertTrue(type(testPlayer.strength), type(0))
        self.assertTrue(type(testPlayer.perception), type(0))
        self.assertTrue(type(testPlayer.enduance), type(0))
        self.assertTrue(type(testPlayer.agility), type(0))
        self.assertTrue(type(testPlayer.posistion), type(""))


class Test_teamModual(unittest.TestCase):

    def test_method_init(self):
        """
            we need to the test the init function to make sure that
            ---
            Dose the depency injection go to right place
            Do all the class attbutes are defined and are the right type
            all attbutes should be blank exspet
         """
        # playerObject = Player()
        teamObject = Team(Player)

        self.assertTrue(type(teamObject.name), type(''))
        # self.assertTrue(len(teamObject.name), len(""))

        self.assertTrue(type(teamObject.keeper), type({}))
        self.assertTrue(type(teamObject.seeker), type({}))
        self.assertTrue(type(teamObject.beater1), type({}))
        self.assertTrue(type(teamObject.beater2), type({}))
        self.assertTrue(type(teamObject.chaser1), type({}))
        self.assertTrue(type(teamObject.chaser2), type({}))
        self.assertTrue(type(teamObject.chaser3), type({}))

    def test_method_genPlayer(self):
        """
            test the genPlayer method but the content function is tested in
            another unit test.
        """
        teamObject = Team(Player)

        self.assertTrue(type(teamObject.genPlayer()), type({}))
        pass

    def test_method_generateTeam(self):
        teamObject = Team(Player)
        team

if __name__ == '__main__':
    unittest.main()
