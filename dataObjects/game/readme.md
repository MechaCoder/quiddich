# game Modual

the game modual is there to run the game, it requires two team upon instanceation and has a debendcy of the random modual from the basic python libary.

## use
if you need to instanceate the modual the Class is called `GameCore` that takes two teams, i would personaly recermend use the appponing team modual but you can use datasource to store information.

``` python
teamOne = team.Team()
teamTwo = team.Team()


teamOne.generateTeam("Ravenclaw")
teamTwo.generateTeam("Griffindor")

gameOject = game.GameCore(teamOne.exportTeam(), teamTwo.exportTeam())
```

## project convetion

|Convetion|Details|
|-|-|
|Events in Game Modual | events are actions that change the state of the game, events are defined by adding methods to `Gamecode` or by inherting the class, you must and defined as an event by the nameing convetion, For a Method to be event the method name must start with  `event_` this will eable the parser to recornise the method as an event the secound part is to define event as being common, uncommon or rare, this is imporant as will help the event radomiser decide how offen an envent should be maybe included in the game loop this is done by haveing `_[eventType]_` as the secound part this can be `_common_`, `_uncommon_` or `_rare_`.|


## methods
### GameCore class
|Method Name|Notes|
|---|---|
|`event_common_passing`| This is a event where the player with possession to anouther |
|`event_common_tackle` | This provides the optuity to for a poseession to change the team |
|`event_uncommon_attemptToScore`| the seeker in poseession of the quffle attempts to score |
|`event_rare_catchSnitich` | this gives the team who court the snitch 150 points and must end the match|
|`createLoopPool`| this creates the loopPool witch is a dict made up of common, uncommon and rare events|
|`buildGameLoop`| this build the radomised array of events that needs to happen

# project mangiement
## Terms of Refenace
|Term|defernetion|
|--|--|
|event| a method with in a game class witch change the state of one or more the game class attbutes, acton witch changes the game state|

#Resources
usefull books and Resources to read to gain more information on the project

+ Qudditch though the ages
