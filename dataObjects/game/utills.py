from random import randint


def dictKeyExsits(_strut={}, _key=""):
    try:
        tryVal = _strut[_key]
        return True
    except Exception as e:
        return False


def possessionDisider(_homeTeam={}, _awayTeam={}):
    if dictKeyExsits(
        _homeTeam,
        "possession"
    ) and dictKeyExsits(
        _homeTeam,
        "possession"
    ):

        if _homeTeam['possession']:
            _homeTeam['type'] = 'home'
            return _homeTeam
        elif _awayTeam['possession']:
            _awayTeam['type'] = 'away'
            return _awayTeam


def chaserAtRandom():
    temp = ['chaser1', 'chaser2', 'chaser3']
    return temp[randint(0, 2)]


def perk():
    return randint(0, 50)


def teamAtRandom():
    teamArray = ['home', 'away']
    return teamArray[randint(0, 1)]
