from inspect import getmembers
from random import randint

# import game.utills as utills


def dictKeyExsits(_strut={}, _key=""):
    try:
        tryVal = _strut[_key]
        return True
    except Exception as e:
        return False


def possessionDisider(_homeTeam={}, _awayTeam={}):
    if dictKeyExsits(
        _homeTeam,
        "possession"
    ) and dictKeyExsits(
        _homeTeam,
        "possession"
    ):

        if _homeTeam['possession']:
            _homeTeam['type'] = 'home'
            return _homeTeam
        elif _awayTeam['possession']:
            _awayTeam['type'] = 'away'
            return _awayTeam


def chaserAtRandom():
    temp = ['chaser1', 'chaser2', 'chaser3']
    return temp[randint(0, 2)]


def perk():
    return randint(0, 50)


def teamAtRandom():
    teamArray = ['home', 'away']
    return teamArray[randint(0, 1)]


class GameCore():
    """
    the Core Class for game containg the most basic event methods
    and the holding the state in ganes score and poseession
    """

    teamHome = {
        "score": 0,  # scores for the matach
        "team": {},  # the exportted team object of the the 'team' modual
    }

    teamAway = {
        "score": 0,
        "team": {},
    }

    possession = {
        "name": "annon",  # the dict posesion id for the player in poseession
        "side": "none"  # this onece a game has started shold be home || away
    }

    eventpoolRaw = {
        "common": [],
        "uncommon": [],
        "rare": []
    }
    gameLoop = []  # the array for the game.

    matchEnd = False  # becomes true when the snitch is captured.

    debugClass = False

    def __init__(self, _home, _away):
        self.teamHome['team'] = _home
        self.teamAway['team'] = _away
        pass

    def __str__(self):
        """ outputs a string containing the name of both teams and the score"""
        return "{} : {} vs {} : {}".format(
            self.teamHome['team']['name'],
            self.teamHome['score'],
            self.teamAway['score'],
            self.teamAway['team']['name']
        )

    def event_common_passing(self):
        """ a player can pass from one player to anouther player """
        # prePass = self.possession['name']
        # witch side has the quffle
        if self.possession['side'] == "home":
            players = self.teamHome['team']['team']
            playerToPassTo = chaserAtRandom()
            self.possession['name'] = playerToPassTo

        elif self.possession['side'] == "away":
            players = self.teamAway['team']['team']
            playerToPassTo = chaserAtRandom()
            self.possession['name'] = playerToPassTo
            pass

    def event_common_tackle(self):
        """ this is a oppenent takeing the quffle of the possession """

        # who has the quffle
        if self.possession['side'] == "home":
            hasQuffle = self.teamHome['team']['team'][self.possession['name']]
            # who is going to try and take the quffle.
            tacklerName = chaserAtRandom()
            attempt = self.teamAway['team']['team'][tacklerName]

            hasTotal = (
                hasQuffle['skills']['AGI'] +
                hasQuffle['skills']['END'] +
                perk()
            )

            attempTotal = (
                attempt['skills']['AGI'] +
                attempt['skills']['PER'] +
                perk()
            )

            if attempTotal > hasTotal:
                self.possession['side'] = "away"
                self.name = tacklerName

        elif self.possession['side'] == "away":
            hasQuffle = self.teamAway['team']['team'][self.possession['name']]
            tacklerName = chaserAtRandom()
            attempt = self.teamHome['team']['team'][tacklerName]

            hasTotal = (
                hasQuffle['skills']['AGI'] +
                hasQuffle['skills']['END'] +
                perk()
            )

            attempTotal = (
                attempt['skills']['AGI'] +
                attempt['skills']['PER'] +
                perk()
            )

            if attempTotal > hasTotal:
                self.possession['side'] = "away"
                self.name = tacklerName

        # do they sucssed

    def event_uncommon_attemptToScore(self):
        """ attempting to score """
        # can the possession score agest the Keeper
        if self.possession['side'] == "home":
            keeper = self.teamAway['team']['team']['keeper']
            seeker = self.teamHome['team']['team'][self.possession['name']]
            teamScore = "away"
        elif self.possession['side'] == "away":
            keeper = self.teamHome['team']['team']['keeper']
            seeker = self.teamAway['team']['team'][self.possession['name']]
            teamScore = "home"

        keeperScore = (
            keeper['skills']['END'] +
            keeper['skills']['AGI']
        ) + perk()

        seekerScore = (
            seeker['skills']['AGI'] +
            keeper['skills']['PER']
        ) + perk()

        if seekerScore > keeperScore:
            if teamScore == "away":
                self.teamHome['score'] += 10
                self.possession['side'] = "away"
            elif teamScore == "home":
                self.teamAway['score'] += 10
                self.possession['side'] = "home"
            pass

        # Keeper = [end] + [agi] + [perk]
        # Possession = [agi] + [per] + [perk]

    def event_rare_catchSnitich(self):
        seekersTeam = teamAtRandom()
        if seekersTeam == 'home':
            seeker = self.teamHome['team']['team']['seeker']
        else:
            seeker = self.teamAway['team']['team']['seeker']
        snitich = randint(75, 175)

        seekerVal = (
            seeker['skills']['PER'] +
            seeker['skills']['AGI']
        ) + perk()

        if seekerVal < snitich:
            if seekersTeam == 'home':
                self.teamHome['score'] += 150
                self.matchEnd = True
            elif seekersTeam == 'away':
                self.teamAway['score'] += 150
                self.matchEnd = True

    def createLoopPool(self):
        """Loops though the self object to put Randomised event into a pool"""

        self.eventpoolRaw = {  # blacnks the event pool
            "common": [],
            "uncommon": [],
            "rare": []
        }

        for e in getmembers(self):
            if "event_" in e[0]:
                if "_common_" in e[0]:
                    self.eventpoolRaw['common'].append(e[0])
                elif "_uncommon_" in e[0]:
                    self.eventpoolRaw['uncommon'].append(e[0])
                elif "_rare_" in e[0]:
                    self.eventpoolRaw['rare'].append(e[0])
            pass

    def buildGameLoop(self):
        """ building the randmised game loop """

        self.createLoopPool()  # create a new Loop

        tempLoop = []
        for e in range(0, randint(100, 10000)):
            eventInt = randint(0, 100)  # this needs to stay as is
            if eventInt < 60:
                temp = self.eventpoolRaw['common'][
                    randint(0, len(self.eventpoolRaw['common']) - 1)
                ]
                tempLoop.append(temp)
            elif eventInt < 80:
                temp = self.eventpoolRaw['uncommon'][
                    randint(0, len(self.eventpoolRaw['uncommon']) - 1)
                ]
                tempLoop.append(temp)
            else:  # for the rare events (above 90)
                temp = self.eventpoolRaw['rare'][
                    randint(0, len(self.eventpoolRaw['rare']) - 1)
                ]
                tempLoop.append(temp)
        self.gameLoop = tempLoop

    def runGame(self):
        """ runs the Game returns the Final score """
        # creates the game loop
        self.buildGameLoop()

        # starting poseession
        # teamAtRandom = utills.teamAtRandom()
        # chaserAtRandom = utills.chaserAtRandom

        self.possession['side'] = teamAtRandom()
        self.possession['name'] = chaserAtRandom()
        if "event_rare_catchSnitich" not in self.gameLoop:
            self.gameLoop.append("event_rare_catchSnitich")
        # choses a event method and runs it (witch changes the game state)
        i = 0
        while self.matchEnd is False and i != 5000:
            # choose an event for the event loop
            thisEvent = self.gameLoop[randint(0, len(self.gameLoop) - 1)]
            EventToCall = getattr(self, thisEvent)
            EventToCall()
            # i += 1
        return {
            "sucssess": True,
            "message": "the Final score | {} {} : {} {}".format(
                self.teamHome['team']['name'],
                self.teamHome['score'],
                self.teamAway['team']['name'],
                self.teamAway['score']
            ),
            "data": {
                "Home": self.teamHome,
                "Away": self.teamAway
            }
        }
        pass
