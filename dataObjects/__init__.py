# the Data controler
from dataObjects.team import Team
from dataObjects.player import Player
from dataObjects.game import GameCore


def TeamsByLeague(_league=""):
    data = {
        "hogwarts": ['Gryffindor', 'Ravenclaw', 'Hufflepuff', 'Slytherin'],
        "national": ['test1', 'test2']
    }
    try:
        return data[_league]
        pass
    except Exception as e:
        return []


def getTeam(_teamName=""):
    playerObj = Player()
    teamobj = Team(Player)
    teamobj.generateTeam(_teamName)
    return teamobj.exportTeam()
    pass


def runGame(_homeTeam={}, _awayTeam={}):


    gameObj = GameCore(_homeTeam, _awayTeam)
    # return gameObj.runGame()
    return gameObj.runGame()
    pass
