from random import randint


class Team():
    """
        the basic team class that genrates the team elements
    """

    def __init__(self, _PlayerObj):
        self.playerPlayerClass = _PlayerObj
        pass

    name = ""
    keeper = {}
    seeker = {}
    beater1 = {}
    beater2 = {}
    chaser1 = {}
    chaser2 = {}
    chaser3 = {}

    def genPlayer(self):
        playerI = self.playerPlayerClass()
        playerI.generatePlayer()
        return playerI.getPlayerObject()

    def generateTeam(self, _TeamName=""):
        self.name = _TeamName

        teamDraftKeeper = []
        teamDraftChaser = []
        teamDraftBeater = []
        teamDraftSeeker = []

        for e in range(0, 50):
            temp = self.genPlayer()
            if temp['posistion'] == 'keeper':
                teamDraftKeeper.append(temp)
            elif temp['posistion'] == 'chaser':
                teamDraftChaser.append(temp)
            elif temp['posistion'] == 'beater':
                teamDraftBeater.append(temp)
            elif temp['posistion'] == 'seeker':
                teamDraftSeeker.append(temp)

        self.keeper = teamDraftKeeper[randint(0, len(teamDraftKeeper) - 1)]
        self.seeker = teamDraftSeeker[randint(0, len(teamDraftSeeker) - 1)]
        self.beater1 = teamDraftBeater[randint(0, len(teamDraftBeater) - 1)]
        self.beater2 = teamDraftBeater[randint(0, len(teamDraftBeater) - 1)]
        self.chaser1 = teamDraftChaser[randint(0, len(teamDraftChaser) - 1)]
        self.chaser2 = teamDraftChaser[randint(0, len(teamDraftChaser) - 1)]
        self.chaser3 = teamDraftChaser[randint(0, len(teamDraftChaser) - 1)]

        captinInt = randint(0, 6)

        if captinInt == 0:
            self.keeper["CAPT'N"] = True
        elif captinInt == 1:
            self.keeper["CAPT'N"] = True
        elif captinInt == 2:
            self.beater1["CAPT'N"] = True
        elif captinInt == 3:
            self.beater2["CAPT'N"] = True
        elif captinInt == 4:
            self.chaser1["CAPT'N"] = True
        elif captinInt == 5:
            self.chaser2["CAPT'N"] = True
        elif captinInt == 6:
            self.chaser3["CAPT'N"] = True
        pass

    def exportTeam(self):
        temp = {
            "name": self.name,
            "team": {
                "keeper": self.keeper,
                "seeker": self.seeker,
                "beater1": self.beater1,
                "beater2": self.beater2,
                "chaser1": self.chaser1,
                "chaser2": self.chaser2,
                "chaser3": self.chaser3
            },
        }
        return temp
