$('.teamSelector').on('change', function(){
  // alert( $(this).find('option:selected').attr('value') )
  $thisSelect = $(this)

  teamSelected = $(this).find('option:selected').attr('value')
  $left = $($(this)).closest('.teamSelector').find('.bannerhalf.left')
  $right = $($(this)).closest('.teamSelector').find('.bannerhalf.right')
  if( teamSelected === "Gryffindor" ){

    $left.css('background-color', '#ae0001')
    $right.css('background-color', '#d3a625')

  } else if ( teamSelected === "Ravenclaw" ) {

    $left.css('background-color', '#0e1a40')
    $right.css('background-color', '#5d5d5d')

  } else if ( teamSelected === "Hufflepuff" ){

    $left.css('background-color', '#ecb939')
    $right.css('background-color', '#726255')

  } else if (teamSelected === "Slytherin") {

    $left.css('background-color', '#1a472a')
    $right.css('background-color', '#aaaaaa')

  }

  $.get("/ajax/getTeam/", {name: teamSelected}, function(_data){

    if( _data['sucsess'] === true ){
      $teamLayout = $thisSelect.find('.teamLayout')
      // $teamLayout.find('span').addClass('loaded');

      $teamLayout.find('span').addClass('loaded')

      position = ['keeper', 'seeker', 'beater1', 'beater2', 'chaser1', 'chaser2', 'chaser3'];

      $teamLayout.find('span').css('background-color', 'white');

      for (var i = 0; i < position.length; i++) {

        if( _data['data']['team'][position[i]]["CAPT'N"] === true ){
          $teamLayout.find('#' + position[i]).css('background-color', 'gold');
        } else {
          $teamLayout.find('#' + position[i]).css('background-color', 'white');
        }

        $teamLayout.find('#' + position[i]).attr("data-name", _data['data']['team'][position[i]]['name'] );
        $teamLayout.find('#' + position[i]).attr("data-agi", _data['data']['team'][position[i]]['skills']['AGI'] );
        $teamLayout.find('#' + position[i]).attr("data-per", _data['data']['team'][position[i]]['skills']['PER'] );
        $teamLayout.find('#' + position[i]).attr("data-str", _data['data']['team'][position[i]]['skills']['STR'] );
      }
      $teamLayout.closest('.teamSelector').find('input[name=home]').val( JSON.stringify( _data['data'] ) );
      // traviling home working on my own qudittich sim listening to Harry Potter witch god is smileing ^_^
    1}

  });

});

$('.teamSelector').on('change', function(){

  if (
    $('.teamSelector #homeTeam option:selected').attr('value').length !== 0 &&
    $('.teamSelector #awayTeam option:selected').attr('value').length !== 0 &&
    $('.teamSelector #awayTeam option:selected').attr('value') !== $('.teamSelector #homeTeam option:selected').attr('value')
  ){
    $('button.j_run_game').removeAttr('disabled');
  } else {
    $('button.j_run_game').attr('disabled', 'true');
  }
})


$('button.j_run_game').on('click', function(){
  homeTeamName = $('select#homeTeam option:selected').attr('value');
  awayTeamName = $('select#awayTeam option:selected').attr('value');

  $.get("/ajax/runGame/", {"home": homeTeamName, "away": awayTeamName},
    function(_data){
      console.log(_data);

      if( _data['sucsess'] === true ){
        swal(
          'The Final Score',
          _data['data']['message'],
          'success'
        )
      }

    })
})
